// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var geyserapp = angular.module('starter', ['applabs.services', 'ionic', 'starter.controllers', 'firebase', 'chart.js'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.views.maxCache(0);
  $ionicConfigProvider.backButton.text('');
  $ionicConfigProvider.backButton.previousTitleText(false);
  $ionicConfigProvider.tabs.position('bottom'); // other values: top
  $ionicConfigProvider.tabs.style('standard');
  $ionicConfigProvider.navBar.alignTitle('center');
  $ionicConfigProvider.navBar.positionPrimaryButtons('left');
  $ionicConfigProvider.navBar.positionSecondaryButtons('right');
  $ionicConfigProvider.form.toggle('large');
  $ionicConfigProvider.form.checkbox('circle');
  $ionicConfigProvider.views.transition('ios');
  $ionicConfigProvider.views.swipeBackEnabled(false);

  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
  .state('app.start', {
    url: '/start',
    views: {
      'menuContent': {
        templateUrl: 'templates/start.html',
        controller: 'StartCtrl'
      }
    }
  })
    .state('admin', {
      url: '/admin',
      abstract: true,
      templateUrl: 'templates/menu_admin.html',
      controller: 'AdminCtrl',
      resolve: {
        auth: function(Auth){
          return Auth.$waitForSignIn();
        }
      }
    })
    .state('admin.landing', {
      url: '/landing',
      views: {
      'menuContent': {
          templateUrl: 'templates/admin.html',
          controller: 'LandingCtrl'
        }
      }
    })
    .state('admin.settings', {
      url: '/settings',
      views: {
      'menuContent': {
          templateUrl: 'templates/admin-settings.html',
          controller: 'SettingsCtrl'
        }
      }
    })
    .state('admin.delete', {
      url: '/delete',
      views: {
      'menuContent': {
          templateUrl: 'templates/admin-delete.html',
          controller: 'DeleteCtrl'
        }
      }
    })
    .state('admin.geyser', {
      url: '/geyser',
      views: {
      'menuContent': {
          templateUrl: 'templates/geyser.html',
          controller: 'GeyserCtrl'
        }
      }
    })
    .state('admin.geyser-add', {
      url: '/geyser-add',
      views: {
      'menuContent': {
          templateUrl: 'templates/geyser-add.html',
          controller: 'GeyserAddCtrl'
        }
      }
    })
    .state('admin.geyser-detail', {
      url: '/geyser/:id',
      views: {
      'menuContent': {
          templateUrl: 'templates/geyser-detail.html',
          controller: 'GeyserDetailCtrl',
          resolve: {
            geyserObj: function(geyserService, $stateParams) {
              return geyserService.getGeyser($stateParams.id);
            }
          }
        }
      }
    })
    .state('admin.geyser-temp', {
      url: '/geyser/:id/temp',
      views: {
      'menuContent': {
          templateUrl: 'templates/geyser-temp.html',
          controller: 'GeyserTempCtrl',
          resolve: {
            geyserObj: function(geyserService, $stateParams) {
              return geyserService.getGeyser($stateParams.id);
            }
          }
        }
      }
    })
    .state('admin.geyser-timetable', {
      url: '/geyser/:id/timetable',
      views: {
      'menuContent': {
          templateUrl: 'templates/geyser-timetable.html',
          controller: 'GeyserTimetableCtrl',
          resolve: {
            geyserObj: function(geyserService, $stateParams) {
              return geyserService.getGeyser($stateParams.id);
            }
          }
        }
      }
    })
    .state('admin.geyser-time', {
      url: '/geyser/:id/time',
      views: {
      'menuContent': {
          templateUrl: 'templates/geyser-time.html',
          controller: 'GeyserTimeCtrl',
          resolve: {
            geyserObj: function(geyserService, $stateParams) {
              return geyserService.getGeyser($stateParams.id);
            }
          }
        }
      }
    })
    .state('admin.geyser-alerts', {
      url: '/geyser/:id/alerts',
      views: {
      'menuContent': {
          templateUrl: 'templates/geyser-alerts.html',
          controller: 'GeyserAlertsCtrl',
          resolve: {
            geyserObj: function(geyserService, $stateParams) {
              return geyserService.getGeyser($stateParams.id);
            }
          }
        }
      }
    })
    .state('admin.geyser-stats1', {
      url: '/geyser/:id/stats1',
      views: {
      'menuContent': {
          templateUrl: 'templates/geyser-stats1.html',
          controller: 'GeyserStats1Ctrl',
          resolve: {
            geyserObj: function(geyserService, $stateParams) {
              return geyserService.getGeyser($stateParams.id);
            }
          }
        }
      }
    })
    .state('admin.geyser-stats2', {
      url: '/geyser/:id/stats2',
      views: {
      'menuContent': {
          templateUrl: 'templates/geyser-stats2.html',
          controller: 'GeyserStats2Ctrl',
          resolve: {
            geyserObj: function(geyserService, $stateParams) {
              return geyserService.getGeyser($stateParams.id);
            }
          }
        }
      }
    })
    .state('admin.geyser-stats3', {
      url: '/geyser/:id/stats3',
      views: {
      'menuContent': {
          templateUrl: 'templates/geyser-stats3.html',
          controller: 'GeyserStats3Ctrl',
          resolve: {
            geyserObj: function(geyserService, $stateParams) {
              return geyserService.getGeyser($stateParams.id);
            }
          }
        }
      }
    })
  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })
  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html',
        controller: 'LoginCtrl'
      }
    }
  })
  .state('app.login-forgot', {
    url: '/login-forgot',
    views: {
      'menuContent': {
        templateUrl: 'templates/login-forgot.html',
        controller: 'LoginForgotCtrl'
      }
    }
  })
  .state('app.register', {
    url: '/register',
    views: {
      'menuContent': {
        templateUrl: 'templates/register.html',
        controller: 'RegisterCtrl'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html'
        }
      }
    })
    .state('app.landing', {
      url: '/land',
      views: {
        'menuContent': {
          templateUrl: 'templates/landing.html',
          controller: 'LandingCtrl'
        }
      }
    })
    

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/start');
});
