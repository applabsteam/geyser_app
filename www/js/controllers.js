angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $rootScope, $ionicModal, $timeout, $state, Auth, $ionicPopup) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };
  $scope.register = function() {
    $scope.modal.show();
  };

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    // Simulate a login delay. Remove this and replace with your login
    // code if using a login system
    $timeout(function() {
      $scope.closeLogin();
    }, 1000);
  };


  $scope.auth = Auth;

  // any time auth state changes, add the user data to scope
  $scope.auth.$onAuthStateChanged(function(firebaseUser) {
    $rootScope.user = firebaseUser;
    
    if(firebaseUser){
      console.log("This should not");
      if($state.current.name.toString().indexOf('admin') < 0){
        $state.go('admin.landing');
      }
    }else{
      $state.go('app.start');
    }
  });
  $scope.showAlert = function(msg) {
    var alertPopup = $ionicPopup.alert({
      title: 'Error!',
      template: msg
    });
  }

  $scope.logout = function(){
    Auth.$signOut();
  }
})
.controller('AdminCtrl', function($scope, Auth, $rootScope, $state){
  console.log("Auth", Auth);
  $scope.auth = Auth;

  // any time auth state changes, add the user data to scope
  $scope.auth.$onAuthStateChanged(function(firebaseUser) {
    $rootScope.user = firebaseUser;
    if(firebaseUser){
      if($state.current.name.toString().indexOf('admin') < 0){
        $state.go('admin.landing');
      }
    }else{
      $state.go('app.start');
    }
  });
  $scope.showAlert = function(msg) {
    var alertPopup = $ionicPopup.alert({
      title: 'Error!',
      template: msg
    });
  }

  $scope.logout = function(){
    Auth.$signOut();
  }
})
.controller('GeyserCtrl',function($scope){
  console.log("Geyser");
})
.controller('GeyserAddCtrl',function($scope, $firebaseArray, $rootScope, $firebaseAuth, Auth, $location){
  $scope.error = {};
  //also logs the user out
  $scope.vm = {
    name:"",
    number:""
  }
  
  
  $scope.addGeyser = function(){
    $scope.error = {};
    var userRef = firebase.database().ref().child("user_geysers").child(Auth.$getAuth().uid).child("geysers").child($scope.vm.number);
    $scope.userGeysers = $firebaseArray(userRef);
    $scope.userGeysers.$add({
      $id: $scope.vm.number,
      id: $scope.vm.number,
      name: $scope.vm.name
    }).then(function(){
      $location.path('/admin/landing');
    });
    console.log($scope.userGeysers);
    
  }
})
.controller('LandingCtrl',function($scope, $firebaseArray, $rootScope, $firebaseAuth, Auth, geyserService, $state, $location){
  $scope.geysers = [];
  $scope.getGeysers = function(){
    var userRef = firebase.database().ref().child("user_geysers").child(Auth.$getAuth().uid).child("geysers");
    $scope.userGeysers = $firebaseArray(userRef);
    console.log($scope.userGeysers);
    $scope.userGeysers.$loaded(function(){
      $scope.geysers = new Array();
      angular.forEach($scope.userGeysers, function(value, key) {
        $scope.geysers.push(value[Object.keys(value)[0]]);
      });
    });
  }

  $scope.showGeyser = function(geyser){
    geyserService.setGeyser(geyser);
    $location.path('/admin/geyser/'+geyser.id);
  }
  $scope.getGeysers();
})
.controller('LoginCtrl',function($rootScope, $scope, $state, $location, $http, Auth){
  $scope.error = {};
  //also logs the user out
  $rootScope.user = {};
  $scope.vm = {
    email:"",
    password:""
  }
  
  $scope.signin = function(){
    $scope.error = {};
    
    Auth.$signInWithEmailAndPassword($scope.vm.email, $scope.vm.password)
    .then(function(firebaseUser) {
      $scope.message = "User created with uid: " + firebaseUser.uid;
    }).catch(function(error) {
      $scope.error = error;
      console.log(error);
      $scope.$parent.showAlert(error.message);
    });
  }
})
.controller('LoginForgotCtrl',function($rootScope, $scope, $state, $location, $http, Auth){
  $scope.error = {};
  //also logs the user out
  $rootScope.user = {};
  $scope.submitted = false;
  $scope.vm = {
    email:"",
    password:"",
    message:""
  }
  
  $scope.signin = function(){
    $scope.error = {};
    $scope.submitted = true;
    Auth.$sendPasswordResetEmail($scope.vm.email).then(function() {
      $scope.vm.message = "An email has been sent to the registered address.";
    }).catch(function(error) {
      $scope.vm.message = "We cannot find a user with that email address.";
    });
  }

  $scope.done = function(){
    $location.path('/app/login');
  }
})
.controller('RegisterCtrl',function($rootScope, $scope, $state, $location, $http, $ionicPopup, Auth){
  $scope.error = {};
  $scope.submit = false;
  $scope.vm = {
    email:'',
    password:'',
    username:''
  }
  $scope.signup = function(){
    if(!$scope.submit){
      $scope.submit = false;
      $scope.error = {};
      Auth.$createUserWithEmailAndPassword($scope.vm.email, $scope.vm.password)
      .then(function(firebaseUser) {
        $scope.message = "User created with uid: " + firebaseUser.uid;
        // update the user in the users table
        var ref = firebase.database().ref();
        var userRef = ref.child("users").child(firebaseUser.uid);
        //console.log(userRef);
        userRef.set({
            name: getName(firebaseUser.providerData[0]),
            username: $scope.vm.username,
            newUser: true,
            role: 10,
            company: 0
          });
          $scope.submit = false;
          //$scope.$parent.doLogon();
        }).catch(function(error) {
          //$scope.error = error;
          $scope.$parent.showAlert(error);
          console.log($scope.error);
          $scope.submit = false;
        });
    }
    
  }

    

  // find a suitable name based on the meta info given by each provider
  function getName(authData) {
    switch(authData.providerId) {
      case 'password':
        var namestr = authData.email.replace(/@.*/, '')
        var madeupName = namestr.charAt(0).toUpperCase() + namestr.slice(1);
        return madeupName;
      case 'twitter':
        return authData.displayName;
      case 'facebook':
        return authData.displayName;
    }
  }
})
.controller('StartCtrl', function($scope, Auth, $rootScope, $state){

})




.controller('SettingsCtrl', function($scope, Auth, $rootScope, $state){
  $scope.logout = function(){
    Auth.$signOut();
  }
})
.controller('DeleteCtrl', function($scope, $firebaseArray, $rootScope, $firebaseAuth, Auth, geyserService, $state, $location, $ionicPopup){
  $scope.geysers = [];
  $scope.getGeysers = function(){
    var userRef = firebase.database().ref().child("user_geysers").child(Auth.$getAuth().uid).child("geysers");
    $scope.userGeysers = $firebaseArray(userRef);
    console.log($scope.userGeysers);
    $scope.userGeysers.$loaded(function(){
      $scope.geysers = new Array();
      angular.forEach($scope.userGeysers, function(value, key) {
        $scope.geysers.push(value[Object.keys(value)[0]]);
      });
    });
  }

  $scope.deleteGeyser = function(geyser){
    var confirmPopup = $ionicPopup.confirm({
            title: 'Delete',
            template: 'Are you sure you want to delete ' + geyser.name.toUpperCase() + ' geyser? All usage statistics for this geyser will be lost.',
            buttons: [
                {
                    text: 'Yes',
                    scope: $scope,
                    onTap: function(e) {
                        var idx = $scope.geysers.indexOf(geyser);
                        $scope.userGeysers.$remove(idx).then(function(){
                          $scope.geysers = new Array();
                          angular.forEach($scope.userGeysers, function(value, key) {
                            $scope.geysers.push(value[Object.keys(value)[0]]);
                          });
                        });
                    }
                },
                { text: 'No',
                    scope: $scope,
                    onTap: function(e) {
                        
                    }
                }
            ]
        });
    
  }
  $scope.getGeysers();
})
/* detail pages */
.controller('GeyserDetailCtrl', function($scope, Auth, $rootScope, $state, $stateParams, geyserService, geyserObj){
  $scope.id = $stateParams.id;
  $scope.geyser = geyserObj;
})
.controller('GeyserTempCtrl', function($scope, Auth, $rootScope, $state, $stateParams, geyserService, geyserObj){
  $scope.id = $stateParams.id;
  $scope.geyser = geyserObj;

  $scope.vm = {
    temp: 25
  }
})
.controller('GeyserTimetableCtrl', function($scope, Auth, $rootScope, $state, $stateParams, geyserService, geyserObj, rangesService){
  $scope.id = $stateParams.id;
  $scope.geyser = geyserObj;
  $scope.ranges = rangesService.get();


  console.log($scope.range);
})
.controller('GeyserTimeCtrl', function($scope, Auth, $rootScope, $state, $stateParams, geyserService, geyserObj){
  $scope.id = $stateParams.id;
  $scope.geyser = geyserObj;

  $(".dial1").knob({
      'change' : function (v) { console.log(v); },
      'fgColor' : '#33cd5f',
      'bgColor' : '#FFFFFF'
  });
  $(".dial2").knob({
      'change' : function (v) { console.log(v); },
      'fgColor' : '#33cd5f',
      'bgColor' : '#FFFFFF'
  });
})
.controller('GeyserAlertsCtrl', function($scope, Auth, $rootScope, $state, $stateParams, geyserService, geyserObj){
  $scope.id = $stateParams.id;
  $scope.geyser = geyserObj;
})
.controller('GeyserStats1Ctrl', function($scope, Auth, $rootScope, $state, $stateParams, geyserService, geyserObj){
  $scope.id = $stateParams.id;
  $scope.geyser = geyserObj;


  $scope.labels = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];

  $scope.options = {
        scales:
        {
            yAxes: [{
                display: false,
                gridLines: {
                    display:false
                } 
            }],
            xAxes: [{
                gridLines: {
                    display:false
                }
            }]
        }
    
    }
  $scope.colors = [
      { // grey
        backgroundColor: '#007aff',
        borderColor: '#007aff'
      }
    ];

  $scope.data = [
    [28, 48, 40, 19, 86, 27, 90]
  ];
})
.controller('GeyserStats2Ctrl', function($scope, Auth, $rootScope, $state, $stateParams, geyserService, geyserObj){
  $scope.id = $stateParams.id;
  $scope.geyser = geyserObj;
})
.controller('GeyserStats3Ctrl', function($scope, Auth, $rootScope, $state, $stateParams, geyserService, geyserObj){
  $scope.id = $stateParams.id;
  $scope.geyser = geyserObj;
});